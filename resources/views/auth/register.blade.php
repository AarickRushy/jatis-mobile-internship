@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('PIC Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="company_name" class="col-md-4 col-form-label text-md-right">{{ __('Company Name') }}</label>

                            <div class="col-md-6">
                                <input id="company_name" type="text" class="form-control @error('company_name') is-invalid @enderror" name="company_name" value="{{ old('company_name') }}">

                                @error('company_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="service-group" class="col-md-4 col-form-label text-md-right">{{ __('Service Group') }}</label>

                            <div class="col-md-8">
                                <input type="checkbox" id="sg-shared" name="service-group" value="shared">Shared
                                <br/>
                                <input type="checkbox" id="sg-dedicated" name="service-group" value="dedicated">Dedicated
                                <br/>
                                <input type="checkbox" id="sg-onprem" name="service-group" value="onprem">OnPrem
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="service-type" class="col-md-4 col-form-label text-md-right">{{ __('Service Type') }}</label>

                            <div class="col-md-4">
                                <input type="checkbox" id="sg-faq" name="service-type" value="faq">FAQ
                                <br/>
                                <input type="checkbox" id="sg-transactional" name="service-type" value="transactional" disabled>Transactional
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="channels" class="col-md-4 col-form-label text-md-right">{{ __('Channels') }}</label>

                            <div class="col-md-4">
                                <input type="checkbox" id="sg-whatsapp" name="channels" value="whatsapp">Whatsapp
                                <br/>
                                <input type="checkbox" id="sg-telegram" name="channels" value="telegram">Telegram
                                <br/>
                                <input type="checkbox" id="sg-slack" name="channels" 
                                value="slack">Slack
                                <br/>
                                <input type="checkbox" id="sg-coster" name="channels" value="coster">Coster
                                <br/>
                                <input type="checkbox" id="sg-custom" name="channels" value="custom">Custom
                                <br/>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="channel-name" class="col-md-4 col-form-label text-md-right">{{ __('Channel Name') }}</label>

                            <div class="col-md-6">
                                <input id="channel-name" type="text" class="form-control @error('channel-name') is-invalid @enderror" name="channel-name" value="{{ old('channel-name') }}">

                                @error('channel-name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">

                            <div class="g-recaptcha" data-sitekey="6LfqL0kfAAAAACyBQyVWM5xXMyx1CLvnA33wPPTh"></div>
                            <br/>
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection